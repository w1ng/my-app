import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import NavComponent from './components/Nav'
import {HashRouter as Router, Route, Switch, hashHistory} from "react-router-dom"
import MerchantComponent from './components/Merchant'
import AccountsComponent from './components/Account'
import {connect} from "react-redux";
import LoginComponent from './components/Login'
import RolesComponent from './components/Roles'


const appStyle = {
    minWidth: '800px',
    minHeight: '600px'
}

const contentWrapper = {
    textAlign: 'center'
}


class App extends Component {



  render() {
    return (
      <div className="App" style={appStyle}>

              <Router history="{hashHistory}">
                  <div>
                      <NavComponent/>
                      <div className="contentWrapper">
                          <Switch>
                              <Route path="/merchants" component={this.props.employee === false ? LoginComponent : MerchantComponent}/>
                              <Route path="/accounts" component={this.props.employee === false ? LoginComponent : AccountsComponent}/>
                              <Route path="/roles" component={this.props.employee === false ? LoginComponent : RolesComponent}/>
                          </Switch>
                      </div>
                  </div>
              </Router>
      </div>
    );
  }
}

function mapStateToProps(state) {

    return {
        employee: state.employee == null ? false : state.employee
    };
}

export default connect(mapStateToProps)(App);