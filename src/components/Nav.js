import React, { Component } from 'react';
import {Link} from 'react-router-dom'
import  { Redirect } from 'react-router-dom'
import {connect} from "react-redux";
import LoginComponent from "./Login";
import {bindActionCreators} from 'redux'
import {setEmployee} from '../redux/action'

const navStyle = {
    background: '#20232a',
    color: 'white',
    margin: '0',
    minHeight: '50px',
    paddingTop: '15px'
}

const navTitle = {
    fontSize: '20px',
    letterSpacing: '2px',
    marginRight: '100px',
    marginLeft: '20px',
    color: '#61dafb',
    fontWeight: 'bold',
}

const navLinks = {
    fontSize:'15px',
    letterSpacing: '2px',
    marginRight: '30px',
    color: 'white',
    minHeight: '40px'
}

const activeNavLink = {
    fontSize:'15px',
    letterSpacing: '2px',
    marginRight: '30px',
    color: '#61dafb',
    borderBottom: '5px solid #61dafb',
    minHeight: '35px',
    marginBottom: '0'
}

const logoutBtnStyle = {
    marginLeft: '300px',
    height: '20px !important',
    background: 'transparent',
    color: 'white',
    border: '0',
    letterSpacing: '2px'
}


class NavComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            activeLink: ''
        }

    }

    linkClicked(link) {
        this.setState({activeLink: link})
    }

    logout() {
        this.props.setEmployee(null)
    }


    render() {

        return (
            <div>
                <div className="row" style={navStyle}>
                    <div className="col-xs-3">
                        <label style={navTitle}>Rush Widget </label>
                    </div>
                    <Link to="/merchants">
                        <div onClick={this.linkClicked.bind(this, 'merchants')} className="col-xs-3"  style={this.state.activeLink === 'merchants' ? activeNavLink : navLinks}>Merchants</div>
                    </Link>

                    <Link to="/accounts">
                        <div onClick={this.linkClicked.bind(this, 'accounts')} style={this.state.activeLink === 'accounts' ? activeNavLink : navLinks} className="col-xs-3" >Accounts</div>
                    </Link>

                    <Link to="/roles">
                        <div onClick={this.linkClicked.bind(this, 'roles')} style={this.state.activeLink === 'roles' ? activeNavLink : navLinks} className="col-xs-3" >Roles</div>
                    </Link>

                    {this.props.employee === false ? null : <label style={logoutBtnStyle} onClick={this.logout.bind(this)}>Logout</label> }
                </div>

            </div>
        )
    }

}

function mapStateToProps(state) {

    return {
        employee: state.employee == null ? false : state.employee
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        setEmployee: setEmployee
    }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(NavComponent);