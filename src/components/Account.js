import React, { Component } from 'react'
import AppService from '../Service'
import {connect} from 'react-redux'
import ReactDataGrid from 'react-data-grid'
import axios from 'axios'
import ReactDOM from 'react-dom'
import Modal from 'react-modal'

const appService = new AppService()

const accountDivStyle = {
    marginLeft: '50px',
    marginTop: '50px'
}


const modalStyle = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        borderRadius          : '0px',
        background            : '#fff',
        border                : '1px solid black',
        minWidth: '500px',
        minHeight: '300px'
    }
};

const headerStyle = {
    marginLeft: '20px'
}



class AccountComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            merchants: [],
            accounts: [],
            roles: [],
            modalIsOpen: false,
            selectedAccount: {
                name: '', roleId: '', uuid: ''
            }
        }
    }

    componentDidMount() {
        this.getMerchants()
    }

    getMerchants() {
        let token =  this.props.employee.access_token
        console.log(token)
        let _this = this
        let promise = appService.callApi('/merchant', null, 'get', token)
        promise.then(function(resp){
            _this.setState({
                'merchants': resp.data
            });
        })
    }

    getAccounts() {
        let token =  this.props.employee.access_token
        let _this = this
        let merchantId = ReactDOM.findDOMNode(this.refs.merchant).value
        console.log(merchantId)

        let promise = appService.callApi('/account?merchant=' + merchantId, null, 'GET', token)
        promise.then(function(response){
            _this.setState({
                accounts: response.data
            })
        })
    }

    onRowClick(index, data) {
        if (data == undefined) {
            return
        }
        this.setState({
            modalIsOpen: true,
            selectedAccount: {
                name: data.name,
                uuid: data.uuid,
                roleId: data.roleId
            }
        })
    }

    getRoles() {
        let token =  this.props.employee.access_token
        let _this = this;
        let merchantId = ReactDOM.findDOMNode(this.refs.merchant).value
        let promise = appService.callApi('/role?merchant=' + merchantId, null, 'GET', token)
        promise.then(function(response){
            _this.setState({
                roles: response.data
            })
            console.log(_this.state.roles)
        })
    }

    selectMerchant() {
        this.getAccounts()
        this.getRoles()
    }

    closeModal() {
        this.setState({
            modalIsOpen: false
        })
    }

    updateData() {
        let uuid = this.state.selectedAccount.uuid
        let name = this.state.selectedAccount.name
        this.setState({
            selectedAccount: {
                uuid: uuid,
                roleId: ReactDOM.findDOMNode(this.refs.role).value,
                name: name
            }
        })
    }

    updateAccount() {
        let _this = this
        let token =  this.props.employee.access_token
        let data =  {
            'uuid': this.state.selectedAccount.uuid,
            'roleId': this.state.selectedAccount.roleId
        }
        console.log(typeof  data.roleId)
        let promise = appService.callApi('/account', data, 'POST', token)
        promise.then(function (response) {
            alert('Account udpated')
            _this.selectMerchant()
            _this.closeModal()
        })
    }


    render() {
        return(
            <div style={accountDivStyle}>


                <Modal
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.closeModal.bind(this)}
                    style=  {modalStyle}
                    contentLabel="Example Modal"
                >
                    <div  className="account-modal">
                        <div className="row">
                            <label style={headerStyle}>Account Details</label>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="col-lg-6">
                                Name
                            </div>
                            <div className="col-lg-6">
                                <input className="prim-input" type="text" disabled value={this.state.selectedAccount.name} />
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-6">
                                Role
                            </div>
                            <div className="col-lg-6">
                                <select className="prim-select" onChange={this.updateData.bind(this)} ref="role" value={this.state.selectedAccount.roleId} required>
                                    <option value="-1">--select--</option>
                                    {
                                        this.state.roles.map(function(role) {
                                            return <option key={role.roleId}
                                                           value={role.roleId}>{role.name}</option>;
                                        })
                                    }
                                </select>
                            </div>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="col-lg-2">
                            </div>
                            <div className="col-lg-3">
                                <button onClick={this.updateAccount.bind(this)} className="btn-primary">Submit</button>
                            </div>
                            <div className="col-lg-1"></div>
                            <div className="col-lg-3">
                                <button onClick={this.closeModal.bind(this)} className="btn-dark">Cancel</button>
                            </div>
                            <div className="col-lg-2">
                            </div>
                        </div>
                    </div>

                </Modal>

                <div>
                    <select ref="merchant" onChange={this.selectMerchant.bind(this)}>
                        <option value="-1">--Select merchant--</option>
                        {
                            this.state.merchants.map(function(merchant) {
                                return <option key={merchant.id}
                                               value={merchant.id}>{merchant.name}</option>;
                            })
                        }
                    </select>
                </div>
                <br/>
                <div>
                    <ReactDataGrid
                        columns={[
                            { resizable: true, key: 'name', name: 'Name' },
                            { resizable: true, key: 'role', name: 'Role' }
                        ]}
                        rowGetter={rowNumber =>  this.state.accounts[rowNumber] }
                        rowsCount={this.state.accounts.length}
                        minHeight={300}
                        minWidth={700}
                        onRowClick={this.onRowClick.bind(this)}
                    />
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {

    return {
        employee: state.employee == null ? false : state.employee
    };
}


export default connect(mapStateToProps)(AccountComponent);