import React, { Component } from 'react'
import AppService from '../Service'
import ReactDOM from 'react-dom'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {setEmployee} from '../redux/action'

const mainDivStyle = {
    textAlign: 'center',
    width: '1000px'
}

const secDivStyle = {
    minWidth: '600px',
    width: '600px',
    marginLeft: '200px'
}

const headerStyle = {
    fontWeight: 'bold'
}

const appService = new AppService()

class LoginComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            credentials: {
                username: '', password: ''
            }
        }
    }

    login() {
        let username = this.state.credentials.username
        let password = this.state.credentials.password
        let _this = this
        appService.requestToken(username, password)
            .then(function(response) {
                _this.props.setEmployee(response.data)
            })
            .catch(function(err){
                alert('Invalid credentials')
            })

    }

    updateState() {
        this.setState({
            credentials: {
                username: ReactDOM.findDOMNode(this.refs.username).value,
                password: ReactDOM.findDOMNode(this.refs.password).value
            }
        })
    }




    render() {
        return (
            <div style={ mainDivStyle }>
               <div style={ secDivStyle }>
                   <br/>
                   <label style={ headerStyle }> Login required to access this page </label>
                   <hr/>
                   <div className="row">
                       <div className="col-lg-3"></div>
                       <div className="col-lg-3">
                           <label> Username </label>
                       </div>
                       <div className="col-lg-3">
                           <input ref="username" onChange={this.updateState.bind(this)} value={this.state.credentials.username} type="text"/>
                       </div>
                   </div>
                   <br/>
                   <div className="row">
                       <div className="col-lg-3"></div>
                       <div className="col-lg-3">
                           <label> Password </label>
                       </div>
                       <div className="col-lg-3">
                           <input ref="password" onChange={this.updateState.bind(this)}  value={this.state.credentials.password}  type="password"/>
                       </div>
                   </div>
                   <br/><br/>
                   <div className="row">
                       <div className="col-lg-4"></div>
                       <div className="col-lg-5">
                            <button onClick={this.login.bind(this)} className="btn-primary">Login</button>
                       </div>

                   </div>
               </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
    }
}


function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        setEmployee: setEmployee
    }, dispatch);
}


export default connect(mapStateToProps, matchDispatchToProps)(LoginComponent);