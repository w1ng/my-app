import React, {Component} from 'react'
import ReactDataGrid from 'react-data-grid'
import axios from 'axios'
import ReactDOM from 'react-dom'
import Modal from 'react-modal'
import AppService from '../Service'
import {connect} from 'react-redux'

const modalStyle = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        borderRadius          : '0px',
        background            : '#fff',
        border                : '1px solid black',
        minWidth: '500px'
    }
};

const appService = new AppService()

const columns = [
    { resizable: true, key: 'name', name: 'ROLE' }
]

const roleWrapperStyle = {
    paddingLeft: '50px',
    paddingTop: '50px'
}

const cancelBtnStyle = {
    marginLeft: '20px'
}


class RolesComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            merchants: [],
            roles: [],
            modalIsOpen: false,
            selectedRole: {
                name: '',
                screens: []
            },
            screens: []
        }
    }

    componentDidMount() {
        this.getMerchants()
        this.getScreens()
    }


    getMerchants() {
        let token =  this.props.employee.access_token
        console.log(token)
        let _this = this
        let promise = appService.callApi('/merchant', null, 'get', token)
        promise.then(function(resp){
            _this.setState({
                'merchants': resp.data
            });
        })
    }

    getScreens() {
        let _this = this;
        let token =  this.props.employee.access_token
        let merchantId = ReactDOM.findDOMNode(this.refs.merchant).value;

        let promise = appService.callApi('/screens', null, 'get', token)
        promise.then(function(resp){
            _this.setState({
                screens: resp.data
            });

        })
    }

    getRoles() {
        let _this = this;
        let token =  this.props.employee.access_token
        let merchantId = ReactDOM.findDOMNode(this.refs.merchant).value;

        let promise = appService.callApi('/role?merchant='+merchantId, null, 'get', token)
        promise.then(function(resp){
            _this.setState({
                'roles': resp.data
            });

        })
    }

    newRole() {
        let _this = this
        this.setState({
            selectedRole: {
                id: '', name: '', screens: _this.state.screens
            },
            modalIsOpen: true
        })
    }

    postRole() {
        let register           = ReactDOM.findDOMNode(this.refs.REGISTER).checked;
        let member_profile     = ReactDOM.findDOMNode(this.refs.MEMBER_PROFILE).checked;
        let give_points        = ReactDOM.findDOMNode(this.refs.GIVE_POINTS).checked;
        let guest_purchase     = ReactDOM.findDOMNode(this.refs.GUEST_PURCHASE).checked;
        let give_points_ocr    = ReactDOM.findDOMNode(this.refs.GIVE_POINTS_OCR).checked;
        let give_stamps        = ReactDOM.findDOMNode(this.refs.GIVE_STAMPS).checked;
        let pay_with_points    = ReactDOM.findDOMNode(this.refs.PAY_WITH_POINTS).checked;
        let redeem_rewards    = ReactDOM.findDOMNode(this.refs.REDEEM_REWARDS).checked;
        let issue_rewards    = ReactDOM.findDOMNode(this.refs.ISSUE_REWARDS).checked;
        let transaction_view    = ReactDOM.findDOMNode(this.refs.TRANSACTIONS_VIEW).checked;
        let ocr_settings    = ReactDOM.findDOMNode(this.refs.OCR_SETTINGS).checked;
        let offline_transactions    = ReactDOM.findDOMNode(this.refs.OFFLINE_TRANSACTIONS).checked;


        let merchant_id = ReactDOM.findDOMNode(this.refs.merchant).value;
        if (merchant_id == '-1') {
            alert('Please select merchant')
            return;
        }
        let data = {
            'name': this.state.selectedRole.name,
            'merchantId': merchant_id,
            'roleId': this.state.selectedRole.id,
            'screens': [
                {
                    'name': 'REGISTER',
                    'checked': register
                },
                {
                    'name': 'MEMBER_PROFILE',
                    'checked': member_profile
                },
                {
                    'name': 'GIVE_POINTS',
                    'checked': give_points
                },
                {
                    'name': 'GUEST_PURCHASE',
                    'checked': guest_purchase
                },
                {
                    'name': 'GIVE_POINTS_OCR',
                    'checked': give_points_ocr
                },
                {
                    'name': 'GIVE_STAMPS',
                    'checked': give_stamps
                },
                {
                    'name': 'PAY_WITH_POINTS',
                    'checked': pay_with_points
                },
                {
                    'name': 'REDEEM_REWARDS',
                    'checked': redeem_rewards
                },
                {
                    'name': 'ISSUE_REWARDS',
                    'checked': issue_rewards
                },
                {
                    'name': 'TRANSACTIONS_VIEW',
                    'checked': transaction_view
                },
                {
                    'name': 'OCR_SETTINGS',
                    'checked': ocr_settings
                },
                {
                    'name': 'OFFLINE_TRANSACTIONS',
                    'checked': offline_transactions
                }
            ]
        }
        let _this = this;
        let token =  this.props.employee.access_token
        let promise = appService.callApi('/role', data, 'POST', token)
        promise.then(function(response) {
            _this.closeModal()
            _this.getRoles()
            alert('Role updated')
        })
    }

    onRowClick(index, data) {
        if (data == undefined) {
            return
        }
        this.setState({
            modalIsOpen: true,
            selectedRole: {
                id: data.roleId,
                name: data.name,
                screens: data.screens
            }

        })
    }
    closeModal() {
        this.setState({
            modalIsOpen: false,
            selectedRole: {
                id: '', name: '', screens: []
            }

        })
    }

    updateData() {
        let roleName = ReactDOM.findDOMNode(this.refs.role).value
        let id = this.state.selectedRole.id
        let screens = this.state.selectedRole.screens
        this.setState({
            selectedRole: {
                name: roleName,
                id: id,
                screens: screens
            }
        })
    }

    render() {
        return (
            <div style={roleWrapperStyle}>

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onRequestClose={this.closeModal.bind(this)}
                    style=  {modalStyle}
                    contentLabel="Example Modal"
                >
                    <div className="role-modal">
                        <div className="row">
                            <label className="prim-label">Role Details</label>
                        </div>
                        <hr/>
                        <div className="row">
                            <div className="col-lg-3">
                                Name
                            </div>
                            <div className="col-lg-9">
                                <input onChange={this.updateData.bind(this)} type="text" ref="role" value={this.state.selectedRole.name}/>
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-2">
                                Access
                            </div>
                            <div className="col-lg-10">
                                {this.state.selectedRole.screens.map((screen,idx) => {

                                    return (
                                        <div className="row">
                                            <div className="col-lg-8">
                                                {screen.name}
                                            </div>
                                            <div className="col-lg-4">
                                                <input ref={screen.name} className={screen.name} type="checkbox" defaultChecked={screen.checked}/>
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </div>
                        <br/>
                        <div className="row">
                            <div className="col-lg-3"></div>
                            <div className="col-lg-3">
                                <button className="btn-primary" onClick={this.postRole.bind(this)}>Submit</button>
                            </div>


                            <div  className="col-lg-3">
                                <button className="btn-dark" onClick={this.closeModal.bind(this)}>Cancel</button>
                            </div>
                        </div>
                    </div>

                </Modal>
                <div className="row">

                    <div className="col-lg-2">
                        <select className="prim-select" ref="merchant" defaultValue="" required>
                            <option value="-1">--select--</option>
                            {
                                this.state.merchants.map(function(merchant) {
                                    return <option key={merchant.id}
                                                   value={merchant.id}>{merchant.name}</option>;
                                })
                            }
                        </select>
                    </div>
                    <div className="col-lg-1">
                        <button className="btn-primary" onClick={this.getRoles.bind(this)}>Search</button>
                    </div>
                    <div className="col-lg-1">
                        <button style={cancelBtnStyle} className="btn-success" onClick={this.newRole.bind(this)}>Add</button>
                    </div>

                </div>
                <br/>
                <div className="row">
                    <ReactDataGrid
                        columns={columns}
                        rowGetter={rowNumber =>  this.state.roles[rowNumber] }
                        rowsCount={this.state.roles.length}
                        minHeight={300}
                        minWidth={900}
                        onRowClick={this.onRowClick.bind(this)}
                    />
                </div>
            </div>
        )
    }

}
function mapStateToProps(state) {

    return {
        employee: state.employee == null ? false : state.employee
    };
}


export default connect(mapStateToProps)(RolesComponent);