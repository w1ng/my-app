import React, { Component } from 'react'
import ReactDataGrid from "react-data-grid";
import axios from 'axios'
import Modal from 'react-modal'
import ReactDOM from "react-dom";
import AppService from '../Service'
import {connect} from 'react-redux'

const appService = new AppService()

const merchantWrapperStyle = {
    paddingLeft: '50px',
    paddingTop: '50px'
}

const modalStyle = {
    content : {
        top                   : '50%',
        left                  : '50%',
        right                 : 'auto',
        bottom                : 'auto',
        marginRight           : '-50%',
        transform             : 'translate(-50%, -50%)',
        borderRadius          : '0px',
        background            : '#fff',
        border                : '1px solid black',
        minHeight: '600px',
        minWidth: '500px'
    }
};

const headerStyle = {
    fontWeight: 'bold'
}



class MerchantComponent extends Component {

    constructor(props) {
        super(props)

        this.state = {
            merchants: [{
            }],
            modalIsOpen: false,
            selectedMerchant: {
                id: '', name: '', merchantApiKey: '', merchantApiSecret: '', customerApiKey: '', customerApiSecret: ''
            },
            statusList: ['ACTIVE', 'INACTIVE'],
            typeList: ['LOYALTY', 'PUNCHCARD']
        }
    }

    componentDidMount() {
        this.getMerchants()
    }


    onRowClick(index, data) {
        if (data == undefined) {
            return
        }
        let selectedMerchant = {
            id: data.id,
            name: data.name,
            merchantApiKey: data.merchantApiKey,
            merchantApiSecret: data.merchantApiSecret,
            customerApiKey: data.customerApiKey,
            customerApiSecret: data.customerApiSecret,
            status: data.status,
            merchantType: data.merchantType,
            withVk: data.withVk
        }

        this.setState({
            modalIsOpen: true,
            selectedMerchant: selectedMerchant
        })
    }

     getMerchants() {
        let token =  this.props.employee.access_token
        console.log(token)
        let _this = this
        let promise = appService.callApi('/merchant', null, 'get', token)
        promise.then(function(resp){
            _this.setState({
                'merchants': resp.data
            });
        })
    }

    afterOpenModal() {

    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            selectedMerchant: {}
        })
    }

    updateData() {
        this.setState({
            selectedMerchant: {
                id: this.state.selectedMerchant.id,
                name:ReactDOM.findDOMNode(this.refs.name).value,
                merchantApiKey: ReactDOM.findDOMNode(this.refs.merchantApiKey).value,
                merchantApiSecret: ReactDOM.findDOMNode(this.refs.merchantApiSecret).value,
                customerApiKey: ReactDOM.findDOMNode(this.refs.customerApiKey).value,
                customerApiSecret: ReactDOM.findDOMNode(this.refs.customerApiSecret).value,
                status: ReactDOM.findDOMNode(this.refs.status).value,
                merchantType: ReactDOM.findDOMNode(this.refs.type).value,
                withVk: ReactDOM.findDOMNode(this.refs.virtualKeyboard).value
            }
        })
    }

    newMerchant() {
        this.setState({
            modalIsOpen: true
        })
    }

    postMerchant() {
        let token = this.props.employee.access_token

        let data =  {
            'id': this.state.selectedMerchant.id,
            'name': this.state.selectedMerchant.name,
            'merchantApiKey': this.state.selectedMerchant.merchantApiKey,
            'merchantApiSecret': this.state.selectedMerchant.merchantApiSecret,
            'customerApiKey': this.state.selectedMerchant.customerApiKey,
            'customerApiSecret': this.state.selectedMerchant.customerApiSecret,
            'status': this.state.selectedMerchant.status,
            'merchantType': this.state.selectedMerchant.merchantType,
            'classification': 'BASIC',
            'withVk': this.state.selectedMerchant.withVk
        }
        let _this = this
        let promise = appService.callApi('/merchant', data, 'POST', token)
        promise.then(function(response){
            _this.getMerchants()
            _this.closeModal()
            alert('Merchant updated.')
        }).catch(function(response){
            alert(response)
        })
    }


    render() {
        return(
            <div style={merchantWrapperStyle} className="merchantWrapper">

                <Modal
                    isOpen={this.state.modalIsOpen}
                    onAfterOpen={this.afterOpenModal}
                    onRequestClose={this.closeModal.bind(this)}
                    style=  {modalStyle}
                    contentLabel="Example Modal"
                >
                <div>
                    <label style={headerStyle}>Merchant details</label>
                    <hr/>
                    <div className="row">
                        <div className="col-lg-6">
                            <label >Name</label>
                        </div>
                        <div className="col-lg-6">
                            <input ref="name" onChange={this.updateData.bind(this)} type="text" value={this.state.selectedMerchant.name}/>
                        </div>
                    </div>

                    <br/>
                    <div className="row">
                        <div className="col-lg-6">
                            <label>Merchant API Key</label>
                        </div>
                        <div className="col-lg-6">
                            <input ref="merchantApiKey" onChange={this.updateData.bind(this)}  type="text" value={this.state.selectedMerchant.merchantApiKey}/>
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-lg-6">
                            <label>Merchant API Secret</label>
                        </div>
                        <div className="col-lg-6">
                            <input ref="merchantApiSecret" onChange={this.updateData.bind(this)}  type="text" value={this.state.selectedMerchant.merchantApiSecret}/>
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-lg-6">
                            <label>Customer API Key</label>
                        </div>
                        <div className="col-lg-6">
                            <input ref="customerApiKey" onChange={this.updateData.bind(this)}  type="text" value={this.state.selectedMerchant.customerApiKey}/>
                        </div>
                    </div>
                    <br/>
                    <div className="row">
                        <div className="col-lg-6">
                            <label>Customer API Secret</label>
                        </div>
                        <div className="col-lg-6">
                            <input ref="customerApiSecret" onChange={this.updateData.bind(this)}   type="text" value={this.state.selectedMerchant.customerApiSecret}/>
                        </div>
                    </div>

                    <br/>
                    <div className="row">
                        <div className="col-lg-6">
                            <label>Status</label>
                        </div>
                        <div className="col-lg-6">
                            <select className="prim-select" onChange={this.updateData.bind(this)} ref="status" value={this.state.selectedMerchant.status} >
                                <option value="-1">--select--</option>
                                {
                                    this.state.statusList.map(function(status) {
                                        return <option key={status}
                                                       value={status}>{status}</option>;
                                    })
                                }
                            </select>
                        </div>
                    </div><br/>

                    <div className="row">
                        <div className="col-lg-6">
                            <label>Package</label>
                        </div>
                        <div className="col-lg-6">
                            <select className="prim-select" onChange={this.updateData.bind(this)} ref="type" value={this.state.selectedMerchant.merchantType} >
                                <option value="-1">--select--</option>
                                {
                                    this.state.typeList.map(function(type) {
                                        return <option key={type}
                                                       value={type}>{type}</option>;
                                    })
                                }
                            </select>
                        </div>
                    </div><br/>

                    <div className="row">
                        <div className="col-lg-6">
                            <label>Virtual Keyboard</label>
                        </div>
                        <div className="col-lg-6">
                            <select className="prim-select" onChange={this.updateData.bind(this)} ref="virtualKeyboard" value={this.state.selectedMerchant.withVk} >
                                <option value="-1">--select--</option>
                                <option value="true">ON</option>
                                <option value="false">OFF</option>
                            </select>
                        </div>
                    </div><br/>


                    <div className="row">
                        <div className="col-lg-2"></div>
                        <div className="col-lg-4">
                            <button onClick={this.postMerchant.bind(this)} className="btn-primary">Submit</button>
                        </div>
                        <div className="col-lg-4">
                            <button onClick={this.closeModal.bind(this)} className="btn-dark">Close</button>
                        </div>
                    </div>
                </div>
                </Modal>

                <div>
                    <button  onClick={this.newMerchant.bind(this)} className="btn-primary">Create</button>
                </div>
                <br/>

                <ReactDataGrid
                    columns={[{ resizable: true,key: 'name', name: 'Name' },
                        { resizable: true, key: 'merchantType', name: 'Package' },
                        { resizable: true, key: 'status', name: 'Status' }]}
                    rowGetter={rowNumber =>  this.state.merchants[rowNumber] }
                    rowsCount={this.state.merchants.length}
                    minHeight={400}
                    minWidth={1000}
                    onRowClick={this.onRowClick.bind(this)}
                />
            </div>
        )
    }
}
function mapStateToProps(state) {

    return {
        employee: state.employee == null ? false : state.employee
    };
}


export default connect(mapStateToProps)(MerchantComponent);