
export const setEmployee = (payload) => {
    return {
        type: 'SET_EMPLOYEE',
        payload: payload
    }
}