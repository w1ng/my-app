import {combineReducers} from 'redux'
import {EmployeeReducer} from './employee-reducer'

const reducer  = combineReducers({
    employee: EmployeeReducer
});

export default reducer
