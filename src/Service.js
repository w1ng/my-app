import axios from 'axios'

const baseUrl = 'http://localhost:8080/rush-pos-sync'
const oauthUrl = '/oauth/token?username=:username&password=:password&grant_type=password'

const clientId= 'rush_client'
const clientSecret = 'rush_client_secret'


class AppService {


    callApi(url, payload, method, token) {

        let fullUrl = baseUrl + url

        let config = {
            method: method,
            url: fullUrl,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            data: JSON.stringify(payload),
            json: true
        };

        return axios.request(config)


    }

    requestToken(username, password) {
        let basicAuth = btoa(clientId + ":" + clientSecret)
        let url = baseUrl + oauthUrl.replace(":username", username).replace(":password", password)

        let postConfig = {
            method: 'POST',
            url: url,
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + basicAuth
            },
            json: true
        };

        return axios(postConfig)
    }


}

export default AppService